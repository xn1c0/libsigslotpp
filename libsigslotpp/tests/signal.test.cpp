#include <doctest/doctest.h>
#include <fmt/core.h>

#include <libsigslotpp/signal.hpp>
#include <memory>

const std::string ff = "free function";
const std::string mf = "member function";
const std::string smf = "static member function";
const std::string mo = "member operator";
const std::string l = "lambda";
const std::string gl = "generic lambda";

void f() { fmt::print("{}\n", ff); }

struct s {
  void m() { fmt::print("{}\n", mf); }
  static void sm() { fmt::print("{}\n", smf); }
};

struct o {
  void operator()() { fmt::print("{}\n, mo"); }
};

TEST_CASE("slots can be added and removed from the signal") {
  std::shared_ptr<s> d;
  auto lambda = []() { fmt::print("{}\n", l); };
  auto gen_lambda = [](auto &&...a) { fmt::print("{}\n", gl); };

  sigslotpp::Signal<void> sig;

  SUBCASE("connecting slots to signal increases connected slots") {
    auto c1 = sig.connect(f);
    sig.connect(d, &s::m);
    sig.connect(&s::sm);
    auto c2 = sig.connect(o());
    sig.connect(lambda);
    sig.connect(gen_lambda);
    CHECK(sig.connectedSlots() == 6);
  }

  SUBCASE("discconnecting all slots from the signal put size to zero") {
    auto c1 = sig.connect(f);
    auto c2 = sig.connect(o());
    sig.connect(lambda);
    sig.connect(gen_lambda);
    sig.disconnectAll();
    CHECK(sig.connectedSlots() == 0);
  }

  SUBCASE("disconnecting slots from the signal decreases connected slots") {
    auto c1 = sig.connect(f);
    auto c2 = sig.connect(o());
    c1.disconnect();
    CHECK(sig.connectedSlots() == 1);
  }
}

TEST_CASE("signal can be emitted") {
  int x{0};
  auto lambda = [&x](int value) { x = x + value; };

  sigslotpp::Signal<void, int> sig;

  sig.connect(lambda);

  sig.emit(5);
  CHECK(x == 5);
  sig.emit(1);
  CHECK(x == 6);
  sig.emit(-6);
  CHECK(x == 0);
}

int sum = 0;
struct x {
  void f(int i) { sum += i; }
};

TEST_CASE("signal can be automatically disconnected") {
  auto a = std::make_shared<x>();
  sigslotpp::Signal<void, int> sig;
  sig.connect(a, &x::f);
  sig.emit(4);
  sig.emit(3);
  sig.emit(-2);
  CHECK(sum == 5);
  a.reset();
  sig.emit(9);
  CHECK(sum == 5);
  CHECK(sig.connectedSlots() == 0);
}

TEST_CASE("signal connection to slot can be blocked") {
  int x{0};
  auto lambda = [&x](int value) { x = x + value; };

  sigslotpp::Signal<void, int> sig;

  auto c1 = sig.connect(lambda);

  sig.emit(5);
  CHECK(x == 5);
  c1.block();
  sig.emit(1);
  CHECK(x == 5);
  c1.unblock();
  sig.emit(-6);
  CHECK(x == -1);
}
